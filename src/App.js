import React, { useRef, useState } from "react";
import { Grid, Error, Response } from "./components";
import { sendGridState } from "./actions";
import { colors } from "./constants"

const initialBoxes = new Array(25);
initialBoxes.fill({
  color: colors.GREEN,
  selected: false
});

function App() {
  const [ response, setResponse ] = useState();
  const [ error, setError ] = useState();

  return (
    <>
    <Grid initialBoxes={initialBoxes} sendGridState={sendGridState} setError={setError} setResponse={setResponse} />
    {
      response && <Response response={response} />
    }
    {
      error && <Error error={error} />
    }
    </>
  );
}

export default App;

// NOTE: is not possible to send the request because of CORS
// restrictions. A possible solution is to use a CORS proxy
export const sendGridState = boxes => (
  new Promise((resolve, reject) => {
    fetch("https://postman-echo.com/post")
    .then(value => {
      resolve(value);
    })
    .catch(reason => {
      reject(reason);
    })
  })
);

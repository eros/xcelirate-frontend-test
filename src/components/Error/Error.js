import React from 'react';
import { StyledError } from "./Error.styled";

const Error = ({ error }) => {
  return (
    <StyledError>{error}</StyledError>
  );
};

export default Error;

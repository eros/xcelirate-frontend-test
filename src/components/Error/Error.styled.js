import styled from "styled-components";

export const StyledError = styled.div`
  background: #ddaaaa;
  border-radius: 4px;
  color: red;
  margin: 5px;
  padding: 5px;
  width: 182px;
`;

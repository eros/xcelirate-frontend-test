import styled from "styled-components";

export const StyledBox = styled.div`
  background: ${props => props.box && props.box.color};
  width: 36px;
  height: 36px;
  margin: 2px;
  transition: background 300ms;
  ${props => props.box.selected ? `filter: opacity(0.7);` : ``}
`;

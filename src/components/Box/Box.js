import React from 'react';
import PropTypes from 'prop-types';
import { StyledBox } from "./Box.styled";

const Box = ({ applySelection, box, index, onMouseEnterBox, onSelected, setColumn }) => {
  const onMouseDown = () => {
    onSelected(index);
  };
  const onMouseUp = () => {
    applySelection();
  };
  const onMouseEnter = () => {
    onMouseEnterBox(index);
  }
  const onDoubleClick = () => {
    setColumn(index);
  }

  return (
    <StyledBox box={box} onMouseDown={onMouseDown} onMouseUp={onMouseUp} onMouseEnter={onMouseEnter} onDoubleClick={onDoubleClick} />
  );
};

Box.propTypes = {
};

export default Box;

import React from 'react';
import { StyledResponse } from "./Response.styled";

const Response = ({ response }) => {
  return (
    <StyledResponse>{response}</StyledResponse>
  );
};

export default Response;

import styled from "styled-components";

export const StyledResponse = styled.div`
  background: #aaddaa;
  border-radius: 4px;
  color: green;
  margin: 5px;
  padding: 5px;
  width: 182px;
`;

import styled from "styled-components";

export const StyledGrid = styled.div`
  display: flex;
  background: #eeeeee;
  flex-wrap: wrap;
  width: 200px;
  height: 200px;
`;

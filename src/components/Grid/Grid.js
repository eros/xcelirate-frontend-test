import React, { useEffect, useRef, useState } from 'react';
import Box from "../Box";
import { colors } from "../../constants"
import { StyledGrid } from "./Grid.styled";

const SQUARE_SIDE = 5;

const getXByIndex = index => {
  return index % SQUARE_SIDE;
};

const getYByIndex = index => {
  return Math.floor(index / SQUARE_SIDE);
};

const getMaxX = (indexes = []) => {
  let max = 0;
  indexes.forEach(i => {
    max = Math.max(max, getXByIndex(i));
  });
  return max;
};

const getMaxY = (indexes = []) => {
  let max = 0;
  indexes.forEach(i => {
    max = Math.max(max, getYByIndex(i));
  });
  return max;
};

const getMinX = (indexes = []) => {
  let min = SQUARE_SIDE - 1;
  indexes.forEach(i => {
    min = Math.min(min, getXByIndex(i));
  });
  return min;
};

const getMinY = (indexes = []) => {
  let min = SQUARE_SIDE - 1;
  indexes.forEach(i => {
    min = Math.min(min, getYByIndex(i));
  });
  return min;
};

const Grid = ({ initialBoxes, sendGridState, setError, setResponse }) => {
  const [ boxes, setBoxes ] = useState(initialBoxes);
  const [ sourceColor, setSourceColor ] = useState();
  let timeout = useRef();

  // calculate rectangle-shaped selction and return box indexes
  const getSelectedRectBoxes = (selectedIndexes, hoveredIndex) => {
    const newBoxes = boxes.map(b => ({ ...b }) );
    const minX = getMinX(selectedIndexes);
    const minY = getMinY(selectedIndexes);
    const maxX = getMaxX(selectedIndexes);
    const maxY = getMaxY(selectedIndexes);
    newBoxes.forEach((b, index) => {
      const x = getXByIndex(index);
      const y = getYByIndex(index);
      // check if the box is inside the rectangle
      if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
        b.selected = true;
      }
    });
    // trim unselected boxes
    const hX = getXByIndex(hoveredIndex);
    const hY = getYByIndex(hoveredIndex);
    if (hX === minX + 1 && hX < maxX) {
      newBoxes.forEach((b, index) => {
        const x = getXByIndex(index);
        if (x === minX) {
          b.selected = false;
        }
      });
    } else if (hX === maxX - 1 && hX > minX) {
      newBoxes.forEach((b, index) => {
        const x = getXByIndex(index);
        if (x === maxX) {
          b.selected = false;
        }
      });
    }
    if (hY === minY + 1 && hY < maxY) {
      newBoxes.forEach((b, index) => {
        const y = getYByIndex(index);
        if (y === minY) {
          b.selected = false;
        }
      });
    } else if (hY === maxY - 1 && hY > minY) {
      newBoxes.forEach((b, index) => {
        const y = getYByIndex(index);
        if (y === maxY) {
          b.selected = false;
        }
      });
    }

    return newBoxes;
  };
  const applySelection = () => {
    const newBoxes = boxes.map(b => ({ ...b }) );
    const selectedBoxes = newBoxes.filter(b => b.selected);
    if (selectedBoxes.length === 1) {
      selectedBoxes.forEach(b => {
        b.color = b.color === colors.GREEN ? colors.RED : colors.GREEN;
        b.selected = false;
      });
    } else {
      selectedBoxes.forEach(b => {
        b.color = sourceColor;
        b.selected = false;
      });
    }
    setBoxes(newBoxes);
    setSourceColor(null);
    if (!timeout.current) {
      timeout.current = setTimeout(() => {
        sendGridState(newBoxes)
        .then(res => {
          setResponse(res);
        })
        .catch(reason => {
          setError(reason.message);
        });
        timeout.current = null;
      }, 2000);
    }
  };
  const onMouseEnterBox = index => {
    const boxSelected = boxes.some(b => b.selected);
    if (boxSelected) {
      // const newBoxes = boxes.map(b => ({ ...b }) );
      // const selectedBoxes = boxes.filter(b => b.selected);
      const selectedIndexes = [];
      boxes.forEach((b, index) => {
        if(b.selected) {
          selectedIndexes.push(index);
        }
      });
      selectedIndexes.push(index); // add the current hovered box to selection
      // const selectedIndexes = getSelectedIndexes()
      setBoxes(getSelectedRectBoxes(selectedIndexes, index));
    }
  };
  const onSelected = index => {
    const newBoxes = boxes.map(b => ({ ...b }) );
    newBoxes[index].selected = true;
    setBoxes(newBoxes);
    setSourceColor(boxes[index].color);
  };
  const setColumn = index => {
    const newBoxes = boxes.map(b => ({ ...b }) );
    const targetColor = boxes[index].color;
    const x = getXByIndex(index);
    newBoxes.forEach((box, index) => {
      if (getXByIndex(index) === x) {
        box.color = targetColor;
      }
    });
    setBoxes(newBoxes);
  };
  return (
    <StyledGrid>
      {boxes.map((b, index) => <Box key={index} applySelection={applySelection} box={b} index={index} onSelected={onSelected} onMouseEnterBox={onMouseEnterBox} setColumn={setColumn} />)}
    </StyledGrid>
  )
  };

export default Grid;
